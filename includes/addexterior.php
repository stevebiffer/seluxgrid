<?php ?>

    <form action="">
        <h1>Vorhandene Varianz Wählen</h1>
        <div class="row">
            <div class="small-12 medium-2 large-2 columns">
                <label for="search">
                    <input type="search" name="search" placeholder="Avanza">
                </label>
            </div>
            <div class="small-12 medium-2 large-2 columns">
                <button class="button secondary">Suche</button>
            </div>
            <div class="small-12 medium-8 large-8 columns">
                <select name="" id="">
                    <option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option><option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option><option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option><option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option><option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option><option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option><option value="">Avanza, rot, Zopf 33, 35W, 4500, nodimm, Flachglas</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-12 medium-6 large-6 columns">
                <button class="button expanded warning">Zerlegen</button>
            </div>
            <div class="small-12 medium-6 large-6 columns">
                <button class="button expanded primary">Freigeben</button>
            </div>
        </div>
    </form>
    <!-- ------------- -->
    <form action="">
        <h1>Neue Varianz hinzufügen</h1>
        <div class="row">

            <div class="medium-4 large-4 columns">
                Artikelnummer + Bezeichnung
                <select name="" id="">
                    <option value="">SX 123 41 - Avanza_neu</option>
                    <option value="">SX 123 42 - Avanza_neu</option>
                    <option value="">SX 123 43 - Avanza_neu</option>
                    <option value="">SX 123 44 - Avanza_neu</option>
                    <option value="">SX 123 45 - Avanza_neu</option>
                </select>
            </div>
            <div class="medium-4 large-4 columns">
                Gehäusefarbe
                <select name="" id="">
                    <option value="">rot</option>
                    <option value="">graphit</option>
                    <option value="">grau</option>
                    <option value="">weiß</option>
                    <option value="">spacegrau</option>
                </select>
            </div>
            <div class="medium-4 large-4 columns">
                Montage
                <select name="" id="">
                    <option value="">Zopf 33</option>
                    <option value="">Zopf 44</option>
                    <option value="">Zopf 55</option>
                    <option value="">Zopf 66</option>
                    <option value="">Zopf 77</option>
                </select>
            </div>

        </div>
        <div class="row">

            <div class="medium-2 large-2 columns">
                Leistung
                <select name="" id="">
                    <option value="">35</option>
                    <option value="">40</option>
                    <option value="">50</option>
                    <option value="">60</option>
                    <option value="">75</option>
                </select>
            </div>
            <div class="medium-2 large-2 columns">
                Lichtfarbe
                <select name="" id="">
                    <option value="">4500</option>
                    <option value="">4501</option>
                    <option value="">4502</option>
                    <option value="">4503</option>
                    <option value="">4504</option>
                </select>
            </div>

            <div class="medium-4 large-4 columns">
                Steuerung
                <input type="text">
            </div>
            <div class="medium-4 large-4 columns">
                Abdeckung
                <input type="text">
            </div>

        </div>


        <div class="row">
            <button class="button expanded primary">Freigeben</button>
        </div>




    </form>